﻿
#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{
	
	}


	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{

	}

	void Show()
	{
		int sum = pow(x, 2) + pow(y, 2) + pow(z, 2);
		std::cout << "\n" << x << " " << y << " " << z << "\n";
		std::cout << "\n" << "Vector Length:" << " " << sqrt(sum) << "\n";
	}
private:
	double x;
	double y;
	double z;
};
int main()
{

	Vector v(10, 5, 11);
	v.Show();
}

